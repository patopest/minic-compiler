%{
#include <stdio.h>
#include <stdlib.h>

int yylex();
void yyerror(const char *s);

extern FILE *fp;
FILE * f1;
%}

%token IDENTIFIER

%token INT VOID CHAR
%token IF ELSE WHILE RETURN STRUCT SIZEOF
%token INCLUDE

%token STRING_LITERAL
%token INT_LITERAL
%token CHAR_LITERAL

%token ASSIGN
%token AND

%token ASTERIX DIV REM
%token PLUS MINUS
%token LT GT LE GE
%token EQ NE

%token LOGAND
%token LOGOR

%%

program 		: include_list structdecl_list vardecl_list fundecl_list
				;


include_list	: include include_list
				| /* empty */
				;
include 		: INCLUDE STRING_LITERAL
				;


structdecl_list	: structdecl structdecl_list
				| /* empty */
				;
structdecl 		: structtype '{' vardecl_plus '}' ';'
				;


vardecl_list	: vardecl vardecl_list
				| /* empty */
				;
vardecl_plus	: vardecl vardecl_plus
				| vardecl
				;
vardecl 		: type IDENTIFIER ';'
				| type IDENTIFIER '[' INT_LITERAL ']' ';'
				;

fundecl_list 	: fundecl fundecl_list
				| fundecl
				;
fundecl 		: type IDENTIFIER '(' params ')' block
				;

type 			: base_type 
				| base_type ASTERIX
				;
base_type 		: INT | VOID | CHAR | structtype
				;
structtype		: STRUCT IDENTIFIER
				;


params 			: type IDENTIFIER other_params
				| /* empty */
				;

other_params 	: other_param other_params
				| /* empty */
				;
other_param 	: ',' type IDENTIFIER
				;


stmt 			: block
				| WHILE '(' exp ')' stmt
				| IF '(' exp ')' stmt else_stmt
				| RETURN ';' | RETURN exp ';'
				| exp ASSIGN exp ';'
				| exp ';'
				;

else_stmt 		: ELSE stmt
				| /* empty */
				;
stmt_list		: stmt stmt_list
				| /* empty */
				;


block 			: '{' vardecl_list stmt_list '}'
				;

exp 			: exp7 exp7_star;
exp7_star 		: LOGOR exp7 exp7_star
				| /* empty */
				;
exp7 			: exp6 exp6_star;
exp6_star 		: LOGAND exp6 exp6_star
				| /* empty */
				;
exp6 			: exp5 exp5_star;
exp5_star 		: EQ exp5 exp5_star
				| NE exp5 exp5_star
				| /* empty */
				;
exp5 			: exp4 exp4_star;
exp4_star		: LT exp4 exp4_star
				| GT exp4 exp4_star
				| LE exp4 exp4_star
				| GE exp4 exp4_star
				| /* empty */
				;
exp4 			: exp3 exp3_star;
exp3_star 		: PLUS exp3 exp3_star
				| MINUS exp3 exp3_star
				| /* empty */
				;
exp3 			: exp2 exp2_star;
exp2_star 		: ASTERIX exp2 exp2_star
				| DIV exp2 exp2_star
				| REM exp2 exp2_star
				| /* empty */
				;
exp2 			: exp2_modif | exp1;
exp2_modif		: PLUS exp
				| MINUS exp
				| '(' type ')' exp
				| ASTERIX exp
				| AND exp
				;
exp1 			: exp0 exp1_star
exp1_star 		: '.' IDENTIFIER exp0
				| '[' exp ']'
				| /* empty */
				;
exp0 			: INT_LITERAL
				| CHAR_LITERAL
				| STRING_LITERAL
				| SIZEOF '(' type ')'
				| '(' exp ')'
				| IDENTIFIER '(' func_args ')'
				;

func_args 		: exp other_args
				;
other_args 		: ',' exp other_args
				| /* empty */
				;

%%

#include "lex.yy.c"

int main(int argc, char *argv[])
{
	return yyparse();
}


void yyerror(const char *s) {
	printf("Syntex Error in line number : %d : %s %s\n", yylineno, s, yytext );
}



