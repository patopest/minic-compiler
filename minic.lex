letter [a-zA-Z]
digit [0-9]

%%
[\t\w]		;
[\n]   { yylineno = yylineno + 1;}

(a-zA-Z_)(a-zA-Z0-9_)* 	return IDENTIFIER;
"="		return ASSIGN;


"int" 	return INT;
"void"	return VOID;
"char"	return CHAR;


"if"		return IF;
"else"		return ELSE;
"while"		return WHILE;
"return"	return RETURN;
"struct"	return STRUCT;
"sizeof"	return SIZEOF;


"#include"	return INCLUDE;


\".*\"  return STRING_LITERAL;
[0-9]+ 	return INT_LITERAL;
\'[a-zA-Z|\t|\b|\n|\r|\f|\'|\\|\0|.|,|_]\' return CHAR_LITERAL;;


"&&" 	return LOGAND;
"||" 	return LOGOR;


"==" 	return EQ;
"!="	return NE;
"<"		return LT;
">" 	return GT;
"<="	return LE;
">=" 	return GE;


"+" 	return PLUS;
"-"		return MINUS;
"*"		return ASTERIX;
"/"		return DIV;
"%"		return REM;
"&"		return AND;


\/\/.* ;
\/\*(.*\n)*.*\*\/ ;

. 		;

%%

/*
"{"		return LBRA;
"}" 	return RBRA;
"("		return LPAR;
")"		return RPAR;
"["		return LSBR;
"]"		return RSBR;
";"		return SC;
"," 	return COMMA;

"."		return DOT;
*/

