.PHONY: build, lex, yacc, clean

.DEFAULT: help
help:
	@echo "make build"
	@echo "       build the compiler"
	@echo "make lex"
	@echo "       build the lexer using flex"
	@echo "make yacc"
	@echo "       build the parser using yacc/bison"
	@echo "make clean"
	@echo "       clean generated files"

lex:
	lex minic.lex

yacc:
	yacc -v minic.y

build: lex yacc
	gcc y.tab.c -ll -ly

clean:
	rm -f lex.yy.c y.tab.c y.output a.out
